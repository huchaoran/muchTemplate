<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Navigation;
use Illuminate\Support\Facades\DB;

class NavigationController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.navigation.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.navigation.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $data = $request->only(['name','icon','link','check','status','sort']);
        isset($data['check'])?$data['check']=1:$data['check']=2;
        isset($data['status'])?$data['status']='active':$data['status']='';
        if($data['status']=='active') {
            if (Navigation::where('status', 'active')->first()) {
                return back()->withErrors('默认页已存在')->with('role', (object)$data);
            }
        }
        if (Navigation::create($data)){
            return redirect()->to(route('admin.navigation.show'))->with(['status'=>'新增导航成功']);
        }
        return redirect()->to(route('admin.navigation.show'))->withErrors('系统错误');
    }

    /**
     * Remove the specified resource from storage.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        $ids = $request->get('ids');
        if (empty($ids)){
            return response()->json(['code'=>1,'msg'=>'请选择删除项']);
        }
        if (Navigation::destroy($ids)){
            return response()->json(['code'=>0,'msg'=>'删除成功']);
        }
        return response()->json(['code'=>1,'msg'=>'删除失败']);
    }

    /**
     * Show the form for editing the specified resource.
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $role = Navigation::findOrFail($id);
        return view('admin.navigation.edit',compact('role'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $role = Navigation::findOrFail($id);
        $data = $request->only(['name','icon','link','check','status','sort']);
        isset($data['check'])?$data['check']=1:$data['check']=2;
        isset($data['status'])?$data['status']='active':$data['status']='';
        if($data['status']=='active'){
            if(Navigation::where('status','active')->first()->id!=$id){
                return back()->withErrors('默认页已存在')->with('role',(object)$data);
            }
        }else{
            if(Navigation::where('status','active')->first()->id==$id){
                return back()->withErrors('默认页必须存在一条')->with('role',(object)$data);
            }
        }
        if ($role->update($data)){
            return redirect()->to(route('admin.navigation.show'))->with(['status'=>'更新导航成功']);
        }
        return redirect()->to(route('admin.navigation.show'))->withErrors('系统错误');
    }

    /**
     * Update the check in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateCheck(Request $request)
    {
        $id = $request->get('id');
        $role = Navigation::findOrFail($id);
        $data['check'] = $role['check']==1?2:1;
        if ($role->update($data)){
            return response()->json(['code'=>0,'msg'=>'更新导航状态成功','icon'=>6]);
        }
        return response()->json(['code'=>1,'msg'=>'更新导航状态失败','icon'=>5]);
    }

    /**
     * Update the status in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateStatus(Request $request)
    {
        $id = $request->get('id');
        DB::update(DB::raw("update navigation set status=''"));
        $role = Navigation::findOrFail($id);
        if ($role->update(['status'=>'active'])){
            return response()->json(['code'=>0,'msg'=>'更新默认页成功','icon'=>6]);
        }
        return response()->json(['code'=>1,'msg'=>'更新默认页成功','icon'=>5]);
    }

    /**
     * show icon list the view
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function icon()
    {
        return view('admin.navigation.icon');
    }
}
