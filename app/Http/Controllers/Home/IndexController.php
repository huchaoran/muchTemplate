<?php

namespace App\Http\Controllers\Home;

use App\Models\Navigation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{

    public function index()
    {
        $arr = Navigation::where('check',1)->orderBy('sort','ASC')->get()->toArray();
        $defaultLink = Navigation::where('status','active')->first(['link'])['link'];
        //显示首页
        return view('home.index.index',['data'=>$arr,'link'=>$defaultLink]);
    }
}
