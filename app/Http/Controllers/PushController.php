<?php

namespace App\Http\Controllers;

class PushController extends Controller
{
    public function push()
    {
        //must be int type(必须是int类型)
        $fd = 1; // Find fd by userId from a map [userId=>fd].
        /**@var \Swoole\WebSocket\Server $swoole */
        $swoole = app('swoole');
        $success = $swoole->push($fd, 'Push data to fd#1 in Controller');
        var_dump($success);
    }
}