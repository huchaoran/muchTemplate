<?php

namespace App\Http\Controllers;

use Hhxsv5\LaravelS\Swoole\Task\Task;

class TaskWorkController extends Task
{
    public $info = [];
    public function __construct()
    {
        $this->info = request()->all();
    }

    /**
     * 异步处理(消息队列)
     */
    public function taskWork()
    {
        $ret = Task::deliver(new TaskWorkController());
        //加入异步任务
        if($ret) {
            echo response()->jsonp('successCallback',[
                'code' => 200,
                'msg' => '处理完成工作~',
                'data' => $this->info
            ])->setCallback(request()->input('successCallback'))->setEncodingOptions(JSON_UNESCAPED_UNICODE);
        }
    }

    /**
     * 处理任务的逻辑，运行在Task进程中，不能投递任务
     * 立即返回数据
     */
    public function handle()
    {
        //立即返回数据
        echo "处理数据中\n";
        sleep(10);//模拟一些需要耗费时间的操作
    }

    /**
     * 任务完成调用 finish 回调时触发，等同于 Swoole 中的 onFinish 逻辑
     * 异步处理数据
     */
    public function finish()
    {
        echo "数据处理完成~\n";
    }
}