
#### 安装前
    支持的环境:
    1.php7以上
    2.mysql5.6以上


#### 安装教程

##### 1. 第一步
    git clone https://gitee.com/huchaoran/muchTemplate.git 
##### 2. 第二步
    composer update 
##### 3. 第三步
    cp .env.example .env
##### 4. 第四步
    php artisan key:generate 
##### 5. 第五步（配置.env的数据库连接）
    php artisan migrate 
##### 5. 第六步
    php artisan db:seed
##### 5. 第七步
    php artisan serve 
 
 #### 使用说明
- 通过url访问`http://localhost:8000/admin`,账号和密码都是`admin`

## 图片展示
- 后台主页 
![Image text](https://raw.githubusercontent.com/github-muzilong/laravel55-layuiadmin/master/public/images/1.png)
- 用户
![Image text](https://raw.githubusercontent.com/github-muzilong/laravel55-layuiadmin/master/public/images/2.png)
- 权限
![Image text](https://raw.githubusercontent.com/github-muzilong/laravel55-layuiadmin/master/public/images/3.png)
- 消息推送
![Image text](https://raw.githubusercontent.com/github-muzilong/laravel55-layuiadmin/master/public/images/4.png)

 ## 新增功能
#### 搭建微服务
- 这里使用的是laravel框架，基于laravel的组件larvels(laravel+swoole)
框架内运行 `php bin/laravels start` 并配置`nginx.conf`include引入一个新的conf文件,为`aa.conf`



        gzip on;
        gzip_min_length 1024;
        gzip_comp_level 2;
        gzip_types text/plain text/css text/javascript application/json application/javascript application/x-javascript application/xml application/x-httpd-php image/jpeg image/gif image/png font/ttf font/otf image/svg+xml;
        gzip_vary on;
        gzip_disable "msie6";
        #nginx upstream用于负载均衡
        upstream swoole {
            # 通过 IP:Port 连接
            #多服务器负载均衡(没有服务器，拿端口来顶替),模拟多个服务器
            server 127.0.0.1:5200 weight=5 max_fails=3 fail_timeout=5s;#可设置参数请求
            server 127.0.0.1:5201 weight=5 max_fails=3 fail_timeout=5s;#可设置参数请求
            # 通过 UnixSocket Stream 连接，小诀窍：将socket文件放在/dev/shm目录下，可获得更好的性能
            #server unix:/yourpath/laravel-s-test/storage/laravels.sock weight=5 max_fails=3 fail_timeout=30s;
            #server 192.168.1.1:5200 weight=3 max_fails=3 fail_timeout=30s;
            #server 192.168.1.2:5200 backup;
            keepalive 16;
        }
        
        server {
            listen       80;
            client_max_body_size 512m;
            server_name  aa.com;
            # root  /Users/huchaoran/Desktop/所有项目/muchTemplate/public;
            autoindex off;
            index index.html index.php;
        
            location / {
                # try_files $uri $uri/ /index.php?$query_string;
                try_files $uri @laravels;
            }
            # location ~ \.php(.*)$ {
            #   	fastcgi_pass   127.0.0.1:9000;
            #    	fastcgi_index  index.php;
            #    	fastcgi_split_path_info  ^((?U).+\.php)(/?.+)$;
            #    	fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
            #    	fastcgi_param  PATH_INFO  $fastcgi_path_info;
            #    	fastcgi_param  PATH_TRANSLATED  $document_root$fastcgi_path_info;
            #   	include        fastcgi_params;
            # }
            # location ~ /\.ht {
            #     deny all;
            # }
            # if (!-e $request_filename) {
            #     rewrite ^/(.*)$ /index.php/$1;
            # }
            location @laravels {
                # proxy_connect_timeout 60s;
                # proxy_send_timeout 60s;
                # proxy_read_timeout 120s;
                proxy_http_version 1.1;
                proxy_set_header Connection "";
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Real-PORT $remote_port;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header Host $http_host;
                proxy_set_header Scheme $scheme;
                proxy_set_header Server-Protocol $server_protocol;
                proxy_set_header Server-Name $server_name;
                proxy_set_header Server-Addr $server_addr;
                proxy_set_header Server-Port $server_port;
                access_log  /Users/huchaoran/Desktop/所有项目/muchTemplate/access.log;
                # “swoole”是指上面的upstream定义的swoole
                proxy_pass http://swoole;
            }
        }
    

 - 配置hosts文件指向到`aa.com`域名，并重载nginx服务器配置，通过`http://aa.com`域名访问
 
 #### 基于swoole定时器
 - `php artisan timer`即可运行，文件在`App\Console\Commands\Timer.php`
 
 #### 新增实时通讯
 - 该功能与微服务合并在一起,文件在`App\Services\WebSocket.php`，案例在`App\Http\PushController.php`
 
 #### 新增异步处理数据(消息队列)
 - 该功能与微服务合并在一起,案例在`App\Http\Controllers\TaskWorkController.php`,接口在`routes\api.php`中的`taskWork`,通过`http://XXXX.com/api/taskWork`进行触发