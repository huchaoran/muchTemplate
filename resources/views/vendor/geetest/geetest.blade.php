<script src="https://cdn.bootcss.com/jquery/2.1.0/jquery.min.js"></script>
<script src="https://static.geetest.com/static/tools/gt.js"></script>
<div id="{{ $captchaid }}"></div>
<p id="wait-{{ $captchaid }}" class="show">正在加载验证码...</p>
@define use Illuminate\Support\Facades\Config
<script>
    var geetest = function(url) {
        var handlerEmbed = function(captchaObj) {
            $("#{{ $captchaid }}").closest('form').submit(function(e) {
                var validate = captchaObj.getValidate();
                if (!validate) {
                    alert('{{ Config::get('geetest.client_fail_alert')}}');
                    e.preventDefault();
                }
            });
            captchaObj.appendTo("#{{ $captchaid }}");
            captchaObj.onReady(function() {
                $("#wait-{{ $captchaid }}")[0].className = "hide";
            });
            if ('{{ $product }}' == 'popup') {
                captchaObj.bindOn($('#{{ $captchaid }}').closest('form').find(':submit'));
                captchaObj.appendTo("#{{ $captchaid }}");
            }
        };
        $.ajax({
            url: url + "?t=" + (new Date()).getTime(),
            type: "get",
            dataType: "json",
            success: function(data) {
                initGeetest({
                    gt: data.gt,
                    challenge: data.challenge,
                    product: "{{ $product?$product:Config::get('geetest.product', 'float') }}",
                    offline: !data.success,
                    new_captcha: data.new_captcha,
                    lang: '{{ Config::get('geetest.lang', 'zh-cn') }}',
                    http: '{{ Config::get('geetest.protocol', 'http') }}' + '://'
                }, handlerEmbed);
            }
        });
    };
    (function() {
        geetest('{{ $url?$url:Config::get('geetest.url', 'geetest') }}');
    })();
</script>
<style>
    .hide {
        display: none;
    }
    #preloader_4 {
        position: relative
    }
    #preloader_4 span {
        position: absolute;
        width: 20px;
        height: 20px;
        background: #3498db;
        opacity: .5;
        border-radius: 20px;
        -webkit-animation: preloader_4 1s infinite ease-in-out;
        -moz-animation: preloader_4 1s infinite ease-in-out;
        -ms-animation: preloader_4 1s infinite ease-in-out;
        -animation: preloader_4 1s infinite ease-in-out
    }

    #preloader_4 span:nth-child(2) {
        left: 20px;
        -webkit-animation-delay: .2s;
        -moz-animation-delay: .2s;
        -ms-animation-delay: .2s;
        animation-delay: .2s
    }

    #preloader_4 span:nth-child(3) {
        left: 40px;
        -webkit-animation-delay: .4s;
        -moz-animation-delay: .4s;
        -ms-animation-delay: .4s;
        animation-delay: .4s
    }

    #preloader_4 span:nth-child(4) {
        left: 60px;
        -webkit-animation-delay: .6s;
        -moz-animation-delay: .6s;
        -ms-animation-delay: .6s;
        animation-delay: .6s
    }

    #preloader_4 span:nth-child(5) {
        left: 80px;
        -webkit-animation-delay: .8s;
        -moz-animation-delay: .8s;
        -ms-animation-delay: .8s;
        animation-delay: .8s
    }
    @-webkit-keyframes preloader_4 {
        0% {
            opacity: .3;
            -webkit-transform: translateY(0px);
            box-shadow: 0 0 3px rgba(0,0,0,.1)
        }

        50% {
            opacity: 1;
            -webkit-transform: translateY(-10px);
            background: #f1c40f;
            box-shadow: 0 20px 3px rgba(0,0,0,.05)
        }

        100% {
            opacity: .3;
            -webkit-transform: translateY(0px);
            box-shadow: 0 0 3px rgba(0,0,0,.1)
        }
    }

    @-moz-keyframes preloader_4 {
        0% {
            opacity: .3;
            -moz-transform: translateY(0px);
            box-shadow: 0 0 3px rgba(0,0,0,.1)
        }

        50% {
            opacity: 1;
            -moz-transform: translateY(-10px);
            background: #f1c40f;
            box-shadow: 0 20px 3px rgba(0,0,0,.05)
        }

        100% {
            opacity: .3;
            -moz-transform: translateY(0px);
            box-shadow: 0 0 3px rgba(0,0,0,.1)
        }
    }

    @-ms-keyframes preloader_4 {
        0% {
            opacity: .3;
            -ms-transform: translateY(0px);
            box-shadow: 0 0 3px rgba(0,0,0,.1)
        }

        50% {
            opacity: 1;
            -ms-transform: translateY(-10px);
            background: #f1c40f;
            box-shadow: 0 20px 3px rgba(0,0,0,.05)
        }

        100% {
            opacity: .3;
            -ms-transform: translateY(0px);
            box-shadow: 0 0 3px rgba(0,0,0,.1)
        }
    }

    @keyframes preloader_4 {
        0% {
            opacity: .3;
            transform: translateY(0px);
            box-shadow: 0 0 3px rgba(0,0,0,.1)
        }

        50% {
            opacity: 1;
            transform: translateY(-10px);
            background: #f1c40f;
            box-shadow: 0 20px 3px rgba(0,0,0,.05)
        }

        100% {
            opacity: .3;
            transform: translateY(0px);
            box-shadow: 0 0 3px rgba(0,0,0,.1)
        }
    }
</style>
