<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>我的首页</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" href="http://g.alicdn.com/msui/sm/0.6.2/css/sm.min.css">
    <link rel="stylesheet" href="{{URL::asset('/static/home/sui/sm-extend.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('/static/home/css/common.css')}}">
</head>
    <body class="theme-dark">
        <div class="page-group">
            <!-- page集合的容器，里面放多个平行的.page，其他.page作为内联页面由路由控制展示 -->
            <div class="page-group">
                <!-- 单个page ,第一个.page默认被展示-->
                <div class="page">
                    <!-- 标题栏 -->
                    {{--<header class="bar bar-nav">--}}
                        {{--<a class="icon icon-me pull-left open-panel"></a>--}}
                        {{--<h1 class="title">标题</h1>--}}
                    {{--</header>--}}

                    <!-- 工具栏 -->
                    <nav class="bar bar-tab">
                        @foreach($data as $k=>$v)
                            <a class="tab-item external {{$v['status']}}" href="#" data='{{$v['link']}}'>
                                <span class="{{$v['icon']}}"></span>
                                <span class="tab-label">{{$v['name']}}</span>
                            </a>
                        @endforeach
                    </nav>
                    <!-- 这里是页面内容区 -->
                    <div class="content">
                        <iframe id='hrefContent' scrolling="auto" src="{{$link}}" width="100%" height="98.7%"></iframe>
                    </div>
                </div>
            </div>
            <!-- popup, panel 等放在这里 -->
            <div class="panel-overlay"></div>
            <!-- Left Panel with Reveal effect -->
            <div class="panel panel-left panel-reveal">
                <div class="content-block">
                    <p>这是一个侧栏</p>
                    <p></p>
                    <!-- Click on link with "close-panel" class will close panel -->
                    <p><a href="#" class="close-panel">关闭</a></p>
                </div>
            </div>
            <!-- <script>$.init()</script> -->
        </div>
        <script type='text/javascript' src='{{URL::asset('/static/home/sui/zepto.min.js')}}' charset='utf-8'></script>
        <script type='text/javascript' src='{{URL::asset('/static/home/sui/sm.min.js')}}' charset='utf-8'></script>
        <script type='text/javascript' src='{{URL::asset('/static/home/sui/sm-extend.min.js')}}' charset='utf-8'></script>
        <script src="{{URL::asset('/js/jquery.min.js')}}"></script>
        <script src="{{URL::asset('/static/home/js/common.js')}}"></script>
    </body>
</html>
