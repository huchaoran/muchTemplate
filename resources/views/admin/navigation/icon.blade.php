<meta name="viewport" content="width=device-width,height=device-height,initial-scale=1.0,user-scalable=no,minimum-scale=1.0,maximum-scale=1.0"/>
<link rel="stylesheet" href="//g.alicdn.com/msui/sm/0.6.2/css/sm.min.css">
<script type='text/javascript' src='//g.alicdn.com/sj/lib/zepto/zepto.min.js' charset='utf-8'></script>
<script type='text/javascript' src='//g.alicdn.com/msui/sm/0.6.2/js/sm.min.js' charset='utf-8'></script>
<script src="{{URL::asset('/js/jquery.min.js')}}"></script>
<script src="{{URL::asset('/static/admin/layuiadmin/layer/layer.js')}}"></script>
<style>
    .color{background-color: #00a2d4;}
    .icon-content{cursor: pointer;float: left;width: 150px;border:1px dashed #000;text-align: center;}
    .icon-content .icon{font-size: 30px;width: 150px;}
    .icon-content .font{font-size: 10px;}
</style>
<div class="content-padded docs-icons">
    <div class="icon-content">
        <span class="icon icon-app"></span>
        <span class="font">icon icon-app</span>
    </div>
    <div class="icon-content">
        <span class="icon icon-browser"></span>
        <span class="font">icon icon-browser</span>
    </div>
    <div class="icon-content">
        <span class="icon icon-card"></span>
        <span class="font">icon icon-card</span>
    </div>
    <div class="icon-content">
        <span class="icon icon-cart"></span>
        <span class="font">icon icon-cart</span>
    </div>
    <div class="icon-content">
        <span class="icon icon-code"></span>
        <span class="font">icon icon-code</span>
    </div>
    <div class="icon-content">
        <span class="icon icon-computer"></span>
        <span class="font">icon icon-computer</span>
    </div>
    <div class="icon-content">
        <span class="icon icon-remove"></span>
        <span class="font">icon icon-remove</span>
    </div>
    <div class="icon-content">
        <span class="icon icon-download"></span>
        <span class="font">icon icon-download</span>
    </div>
    <div class="icon-content">
        <span class="icon icon-edit"></span>
        <span class="font">icon icon-edit</span>
    </div>
    <div class="icon-content">
        <span class="icon icon-emoji"></span>
        <span class="font">icon icon-emoji</span>
    </div>
    <div class="icon-content">
        <span class="icon icon-star"></span>
        <span class="font">icon icon-star</span>
    </div>
    <div class="icon-content">
        <span class="icon icon-friends"></span>
        <span class="font">icon icon-friends</span>
    </div>
    <div class="icon-content">
        <span class="icon icon-gift"></span>
        <span class="font">icon icon-gift</span>
    </div>
    <div class="icon-content">
        <span class="icon icon-phone"></span>
        <span class="font">icon icon-phone</span>
    </div>
    <div class="icon-content">
        <span class="icon icon-clock"></span>
        <span class="font">icon icon-clock</span>
    </div>
    <div class="icon-content">
        <span class="icon icon-home"></span>
        <span class="font">icon icon-home</span>
    </div>
    <div class="icon-content">
        <span class="icon icon-menu"></span>
        <span class="font">icon icon-menu</span>
    </div>
    <div class="icon-content">
        <span class="icon icon-message"></span>
        <span class="font">icon icon-message</span>
    </div>
    <div class="icon-content">
        <span class="icon icon-me"></span>
        <span class="font">icon icon-me</span>
    </div>
    <div class="icon-content">
        <span class="icon icon-picture"></span>
        <span class="font">icon icon-picture</span>
    </div>
    <div class="icon-content">
        <span class="icon icon-share"></span>
        <span class="font">icon icon-share</span>
    </div>
    <div class="icon-content">
        <span class="icon icon-settings"></span>
        <span class="font">icon icon-settings</span>
    </div>
    <div class="icon-content">
        <span class="icon icon-refresh"></span>
        <span class="font">icon icon-refresh</span>
    </div>
    <div class="icon-content">
        <span class="icon icon-caret"></span>
        <span class="font">icon icon-caret</span>
    </div>
    <div class="icon-content">
        <span class="icon icon-down"></span>
        <span class="font">icon icon-down</span>
    </div>
    <div class="icon-content">
        <span class="icon icon-up"></span>
        <span class="font">icon icon-up</span>
    </div>
    <div class="icon-content">
        <span class="icon icon-right"></span>
        <span class="font">icon icon-right</span>
    </div>
    <div class="icon-content">
        <span class="icon icon-left"></span>
        <span class="font">icon icon-left</span>
    </div>
    <div class="icon-content">
        <span class="icon icon-check"></span>
        <span class="font">icon icon-check</span>
    </div>
    <div class="icon-content">
        <span class="icon icon-search"></span>
        <span class="font">icon icon-search</span>
    </div>
</div>
<span style="display: none;" id="value"></span>
<script>
    $('.icon-content').click(function () {
        $('.icon-content').removeClass('color');
        $(this).addClass('color');
        $("#value").html($(this).children(".font").html());
    });
</script>
