@extends('admin.base')

@section('content')
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <h2>添加导航</h2>
        </div>
        <div class="layui-card-body">
            <form action="{{route('admin.navigation.store')}}" method="post" class="layui-form">
                @include('admin.navigation._form')
            </form>
        </div>
    </div>
@endsection
