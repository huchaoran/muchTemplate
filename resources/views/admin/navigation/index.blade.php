@extends('admin.base')

@section('content')
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <div class="layui-btn-group">
                @can('admin.navigation.delete')
                    <button class="layui-btn layui-btn-sm layui-btn-danger" id="listDelete">删 除</button>
                @endcan
                @can('admin.navigation.create')
                    <a class="layui-btn layui-btn-sm" href="{{ route('admin.navigation.create') }}">添 加</a>
                @endcan
                @can('admin.navigation.edit')
                    <a class="layui-btn layui-btn-sm layui-btn-normal" id="listHandleStatus">设置默认页</a>
                @endcan
            </div>
        </div>
        <div class="layui-card-body">
            <table id="dataTable" lay-filter="dataTable"></table>
            <script type="text/html" id="options">
                <div class="layui-btn-group">
                    @can('admin.navigation.edit')
                        <a class="layui-btn layui-btn-sm" lay-event="edit">编辑</a>
                    @endcan
                    @can('admin.navigation.delete')
                        <a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="del">删除</a>
                    @endcan
                </div>
            </script>
        </div>
    </div>
    <script type="text/html" id="isCheck">
        @{{# if (d.check=== 1) { }}
        <a href="javascript:;" lay-event="edit1" style="color: #2ab27b;">开启</a>
        @{{# } else { }}
        <a href="javascript:;" lay-event="edit1" style="color: #411c0e;">关闭</a>
        @{{# } }}
    </script>
    <script type="text/html" id="isStatus">
        @{{# if (d.status == 'active') { }}
        <a href="javascript:;" style="color: #2ab27b;">默认</a>
        @{{# } else { }}
        <a href="javascript:;" style="color: #411c0e;">不默认</a>
        @{{# } }}
    </script>
    <script type="text/html" id="isLink">
        <a href="@{{d.link}}" target="_blank">@{{d.link}}</a>
    </script>
    <script type="text/html" id="isDefault">
        <input type="radio" name="default" value="@{{d.id}}" title="@{{d.name}}" @{{# if (d.status=='active') { }}checked=""@{{# } else { }}@{{# } }}>
    </script>
@endsection

@section('script')
    @can('navigation.manage')
    <script>
        layui.use(['layer','table','form'],function () {
            var layer = layui.layer;
            var form = layui.form;
            var table = layui.table;
            //用户表格初始化
            var dataTable = table.render({
                elem: '#dataTable'
                // ,height: 500
                ,url: "{{ route('admin.data') }}" //数据接口
                ,where:{model:"navigation"}
                ,page: true //开启分页
                ,cols: [[ //表头
                    {checkbox: true,fixed: true}
                    ,{field: 'id', title: 'ID', sort: true}
                    ,{field: 'name', title: '名称'}
                    ,{field: 'icon', title: '图标'}
                    ,{field: 'link', title: '地址',templet: '#isLink'}
                    ,{field: 'check', title: '是否开启',templet: '#isCheck', width: 100}
                    ,{field: 'status', title: '默认页',templet: '#isStatus', width: 100}
                    ,{field: 'sort', title: '排序',width: 70}
                    // ,{field: 'created_at', title: '创建时间'}
                    // ,{field: 'updated_at', title: '更新时间'}
                    ,{fixed: 'right', width: 150, align:'center', toolbar: '#options'}
                ]]
            });

            //监听工具条
            table.on('tool(dataTable)', function(obj){ //注：tool是工具条事件名，dataTable是table原始容器的属性 lay-filter="对应的值"
                var data = obj.data //获得当前行数据
                    ,layEvent = obj.event; //获得 lay-event 对应的值
                if(layEvent === 'del'){
                    layer.confirm('确认删除吗？', function(index){
                        $.post("{{ route('admin.navigation.delete') }}",{_method:'delete',ids:[data.id]},function (result) {
                            if (result.code==0){
                                obj.del(); //删除对应行（tr）的DOM结构
                            }
                            layer.close(index);
                            layer.msg(result.msg,{icon:6})
                        });
                    });
                } else if(layEvent === 'edit'){
                    location.href = '/admin/navigation/'+data.id+'/edit';
                } else if (layEvent === 'permission'){
                    location.href = '/admin/navigation/'+data.id+'/permission';
                } else if(layEvent === 'edit1'){//是否开启
                    @can('admin.navigation.edit')
                        $.post("{{ route('admin.navigation.updateCheck') }}",{_method:'put',id:data.id},function (result) {
                            if (result.code==0){
                                dataTable.reload();
                            }
                            layer.msg(result.msg,{icon:result.icon});
                        });
                        return false;
                    @endcan
                    layer.msg('暂无权限~',{icon:5});
                }
            });

            //按钮批量删除
            $("#listDelete").click(function () {
                var ids = []
                var hasCheck = table.checkStatus('dataTable')
                var hasCheckData = hasCheck.data
                if (hasCheckData.length>0){
                    $.each(hasCheckData,function (index,element) {
                        ids.push(element.id)
                    })
                }
                if (ids.length>0){
                    layer.confirm('确认删除吗？', function(index){
                        $.post("{{ route('admin.navigation.delete') }}",{_method:'delete',ids:ids},function (result) {
                            if (result.code==0){
                                dataTable.reload()
                            }
                            layer.close(index);
                            layer.msg(result.msg,{icon:6})
                        });
                    })
                }else {
                    layer.msg('请选择删除项',{icon:5})
                }
            })

            //设置列表的默认页
            $("#listHandleStatus").click(function () {
                var load = layer.load();
                layer.open({
                    type:1, title:'设置默认页', area : ['80%','80%'],
                    content:'<div><table id="defaultStatus"></table></div>',
                    success : function(index, layero) {
                        layer.close(load);
                        table.render({
                            elem: '#defaultStatus'
                            ,url: "{{ route('admin.data') }}" //数据接口
                            ,where:{model:"navigation"}
                            ,page: true //开启分页
                            ,cols: [[ //表头
                                {title:'默认页',fixed: true,width:100,templet:'#isDefault'}
                                ,{field: 'id', title: 'ID', sort: true,width:50}
                                ,{field: 'icon', title: '图标'}
                                ,{field: 'link', title: '地址'}
                            ]]
                        });
                    },
                    btn : [ '确定', '关闭' ],
                    yes : function(index, layero) {
                        var checkStatus = $("input[name='default']:checked").val();
                        $.post("{{ route('admin.navigation.updateStatus') }}",{_method:'put',id:checkStatus},function (result) {
                            layer.msg(result.msg,{icon:result.icon});
                        });
                        layer.close(index);
                        dataTable.reload();
                    }
                });
            });
        })
    </script>
    @endcan
@endsection
