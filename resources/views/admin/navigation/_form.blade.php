{{csrf_field()}}
@if(!empty(session('role')))
    <?php $role=session('role');?>
@endif
<div class="layui-form-item">
    <label for="" class="layui-form-label">名称</label>
    <div class="layui-input-block">
        <input class="layui-input" type="text" name="name" lay-verify="required" value="{{$role->name??old('name')}}" placeholder="如:首页">
    </div>
</div>
<div class="layui-form-item">
    <label for="" class="layui-form-label">图标</label>
    <div class="layui-input-block">
        <a class="layui-btn layui-btn-sm layui-btn-normal" id="getIconList">选择图标</a>
            <input class="layui-input" readonly="" id="icon" type="text" name="icon" value="{{$role->icon??old('icon')}}" placeholder="如：图标" >
    </div>
</div>
<div class="layui-form-item">
    <label for="" class="layui-form-label">链接</label>
    <div class="layui-input-block">
        <input class="layui-input" type="text" name="link" lay-verify="required" value="{{$role->link??old('link')}}" placeholder="如：项目地址直接 '/home/index' 进行访问" >
    </div>
</div>
<div class="layui-form-item">
    <label for="" class="layui-form-label">状态</label>
    <div class="layui-input-block">
        <input type="checkbox" {{isset($role->check)?$role->check==1?'checked=""':old('check'):old('check')}} name="check" lay-skin="switch" lay-filter="switchTest" lay-text="ON|OFF">
    </div>
</div>
<div class="layui-form-item">
    <label for="" class="layui-form-label">是否默认页</label>
    <div class="layui-input-block">
        <input type="checkbox" {{isset($role->status)?$role->status=='active'?'checked=""':old('status'):old('status')}} name="status" lay-skin="switch" lay-filter="switchTest" lay-text="是|否">
    </div>
</div>
<div class="layui-form-item">
    <label for="" class="layui-form-label">排序</label>
    <div class="layui-input-block">
        <input class="layui-input" type="text" name="sort" lay-verify="required" value="{{$role->sort??1}}" onkeyup="if(isNaN(value)){execCommand('undo');layer.msg('只能输入数字 !');}" maxlength="2" onafterpaste="if(isNaN(value))execCommand('undo')"  placeholder="排序" >
    </div>
</div>
<div class="layui-form-item">
    <div class="layui-input-block">
        <button type="submit" class="layui-btn" lay-submit="" >确 认</button>
        <a href="{{route('admin.navigation.show')}}" class="layui-btn" >返 回</a>
    </div>
</div>
<script src="{{URL::asset('/js/jquery.min.js')}}"></script>
<script>
    $("#getIconList").click(function () {
        layer.open({type:2, title:'设置图标', area : ['800px', '550px'],shadeClose:true,shade:0.8,fix:false, btn: ['确认', '关闭'], content: ["{{url('iconList')}}",'yes'],
            yes:function (index, layero) {
                var icon = layer.getChildFrame('body', index).find('#value').html();
                $("#icon").attr('value',icon);
                layer.close(index);//需要手动关闭窗口
            }});
    });
</script>
