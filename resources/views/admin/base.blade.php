
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>控制台主页一</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="/static/admin/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/static/admin/layuiadmin/style/admin.css" media="all">
</head>
<body>

<div class="layui-fluid">
    @yield('content')
</div>

<script src="/js/jquery.min.js"></script>
<script src="/static/admin/layuiadmin/layui/layui.js"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    layui.config({
        base: '/static/admin/layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['element','form','layer','table','upload','laydate'],function () {
        var element = layui.element;
        var layer = layui.layer;
        var form = layui.form;
        var table = layui.table;
        var upload = layui.upload;
        var laydate = layui.laydate;

        //错误提示
        @if(count($errors)>0)
            @foreach($errors->all() as $error)
                layer.msg("{{$error}}",{icon:5});
                @break
            @endforeach
        @endif

        //信息提示
        @if(session('status'))
            layer.msg("{{session('status')}}",{icon:6});
        @endif
    });
    //监听消息推送(socket.io监听事件)
    var sockio =()=> {
        var url = "ws://{{config('laravels.listen_ip').':'.config('laravels.listen_port')}}"+"?uid="+"{{auth()->user()->uuid}}";
        console.log(url);
        //兼容 FireFox
        if ("WebSocket" in window) {
            socket = new WebSocket(url);
        } else if ("MozWebSocket" in window) {
            socket = new MozWebSocket(url);
        }
        socket.onopen = function(event) {
            // socket.send("已连接~");
        };
        socket.onmessage = function(event) {//当客户端收到服务端发来的消息时，会触发onmessage事件，参数event.data中包含server传输过来的数据
            var content = event.data;//获取消息
            layer.open({
                title: content,
                content: content,
                shade: false,
                anim: 1,
            });
            layer.msg(content,{time: 1000});
        };
        socket.onclose = function(evt)
        {
            // console.log("WebSocketClosed!");
        };
        socket.onerror = function(evt)
        {
            // console.log("WebSocketError!");
        };
    };
    setInterval("sockio()",590000);
</script>
@yield('script')
</body>
</html>



