<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNavigationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('navigation', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',10)->comment('名称');
            $table->string('icon',20)->default('')->comment('icon');
            $table->string('link',255)->comment('链接');
            $table->integer('check')->default(2)->comment('状态 1.开启  2.关闭(默认)');
            $table->string('status',6)->default('')->comment('两种 1.active  2.为空');
            $table->tinyInteger('sort')->default(1)->comment('排序');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('navigation');
    }
}
